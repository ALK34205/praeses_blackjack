/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alec Turner
 */
public class Player {
    /**
     * Player's name
     */
    private String name;
    
    /**
     * The maximum number of cards a player can have in hand.
     */
    private static final int MAX_HAND_SIZE = 10;
    
    /**
     * Cards in the player's current hand.
     */
    private Card[] hand = new Card[MAX_HAND_SIZE];
    
    /**
     * Number of cards in the player's current hand.
     */
    private int numCards;
    
    /**
     * Constructor for a player.
     * @param aName player name
     */
    public Player(String aName)
    {
        this.name = aName;
        
        //empty hand
        this.emptyHand();       
    }
    
    /**
     * Reset the player's hand to 0.
     */
    public void emptyHand()
    {
        for (int c = 0; c < MAX_HAND_SIZE; c++)
        {
            this.hand[c] = null;            
        }
        this.numCards = 0;
    }
    
    /**
     * Add a card to the player's hand  
     * @param aCard card added
     * @return whether the sum of the new hand is below or equal to 21
     */
    public boolean addCard(Card aCard)
    {
        //print error if hand has maximum amount of cards
        if (this.numCards == MAX_HAND_SIZE) 
        {
            System.err.printf("%s's hand already has the maximum amount of "
                    + "cards; " + "cannot add another\n", this.name);
            System.exit(1); 
        }
        
        //add a new card and increment number of cards counter
        this.hand[this.numCards] = aCard;
        this.numCards++;
        
        return (this.getHandSum() <= 21);
    }
    /**
     * Method for getting a card from the player's hand
     * @param i position in the hand[] to return the card from
     * @return card at position i in the hand[]
     */
    public Card getCard(int i)
    {
        return hand[i];
    }
    
    public int getHandSum()
    {
        int handSum = 0; 
        int numAces = 0;
        int cardNum;
        
        //calculate each card's contribution to the hand sum
        for (int c = 0; c < this.numCards; c++)
        {
            //value of current card
            cardNum = this.hand[c].getValue();
            
            if (cardNum == 1) 
            {
                numAces++;
                handSum += 11;
            }else if (cardNum > 10)
            {
                handSum += 10;
            }else
            {
                handSum += cardNum;
            }
        }
        
        while(handSum > 21 && numAces > 0)
        {
            handSum -= 10;
            numAces--;
        }
        
        return handSum;
    }
    

    /**
     * Method for returning the name of the player
     * @return the player name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Method for returning a player's hand as a string
     * @return the string representation of the cards in the player's hand
     */
    public String getHand()
    {
        String cardsInHand = "";
        boolean showFirstCard = true;
        
        System.out.printf("%s's cards:\n", this.name);
        for (int c = 0; c < this.numCards; c++)
        {
            if (c == 0 && !showFirstCard) 
            {
                System.out.println("  [hidden]");
            }else
            {
                System.out.printf("  %s\n", cardsInHand = this.hand[c].toString());
            }      
        }
        return cardsInHand;
    }
        

    /**
     * Print the cards in the player's hand.
     * @param showFirstCard whether the first card is hidden or not
     */
    public void printHand(boolean showFirstCard)
    {
        System.out.printf("%s's cards:\n", this.name);
        for (int c = 0; c < this.numCards; c++)
        {
            if (c == 0 && !showFirstCard) 
            {
                System.out.println("  [hidden]");
            }else
            {
                System.out.printf("  %s\n", this.hand[c].toString());
            }
        }
    }
}

