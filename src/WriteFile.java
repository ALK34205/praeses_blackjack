/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

/**
 *
 * @author Alec Turner
 */
public class WriteFile {
    private String path;
    private boolean appendToFile = false;
    
    public WriteFile(String filePath)
    {
        path = filePath;
    }
    
    public WriteFile(String filePath, boolean appendValue)
    {
        path = filePath;
        appendToFile = appendValue;
    }
    
    public void writeToFile(String textLine) throws IOException
    {
        FileWriter write = new FileWriter(path, appendToFile);
        
        PrintWriter printLine = new PrintWriter(write);
        
        printLine.printf("%s" + "%n", textLine);
        
        printLine.close();
    }
}
