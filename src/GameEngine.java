/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.JFrame;
/**
 *
 * @author Alec Turner
 */
public class GameEngine extends JFrame{
    
    public GameEngine()
    {
        GameBoard gameBoard = new GameBoard();
        add(gameBoard);
        setSize(950, 950);
    }
    
     public static void main(String[] args) {

        GameEngine game = new GameEngine();
        game.setLocationRelativeTo(null);
        game.setVisible(true);
        
    } 
}
