
import java.awt.Image;
import javax.swing.ImageIcon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alec Turner
 */
public class Card {
   
    /**
     * Suit value, and card value (Ace - King)
     */
    private Suit mySuit;
    private int myValue;
    private ImageIcon backImage;
    private ImageIcon image;
    private boolean isFlipped = false;
    
    /**
     * Card constructor that takes in a suit and a value (as an integer)
     * @param aSuit
     * @param aValue 
     */
    public Card(Suit aSuit, int aValue)
    {
        this.mySuit = aSuit;
        
        if (aValue >= 1 && aValue <= 13)
        {
            this.myValue = aValue;
        } else 
        {
            System.err.println(aValue + " is not valid");
            System.exit(1);
        }
        
        backImage = new ImageIcon(getClass().getResource("resources/cardBack.jpg"));
    }
    
    

    
    /**
     * List of available suits.
     */
    public enum Suit 
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades,
    }
    
    public int getValue()
    {
        return myValue;
    }
    
    public Suit getSuit()
    {
        return mySuit;
    }
    
    /**
     * Method for getting the card image based off the Suit and Value of the card
     * @return the image for the card
     */
    public ImageIcon getImage()
    {     
         /**
          * An attempt to implement a backImage for the cards using a boolean
          */
//        if(isFlipped)
//        {
//            image = new ImageIcon(getClass().getResource("resources/cardBack.jpg")); 
//        }
//        else{
            image = new ImageIcon(getClass().getResource("resources/" + mySuit + "" + myValue + ".jpg"));
//        }
        return image;
    }
        /**
         * An attempt at a method for setting the boolean to decide whether or not to use the card back image
         */
//    public void flip()
//    {
//        if(isFlipped)
//        {
//            isFlipped = false;
//        }
//        else{
//            isFlipped = true;
//        }
//        
//        
//    }
    
    /**
     * Override of toString() prints out a String based on the integer value
     * received. Initialized as "Error" as a flag that something is wrong 
     * if "Error" is printed.
     * @return a String ex: "8 of Hearts"
     */
    public String toString()
    {
        String numStr = "Error";
        
        switch(this.myValue)
        {
            case 1:
                numStr = "Ace";
                break; 
            case 2:
                numStr = "Two";
                break;
            case 3:
                numStr = "Three";
                break;
            case 4:
                numStr = "Four";
                break;
            case 5:
                numStr = "Five";
                break;
            case 6:
                numStr = "Six";
                break;
            case 7:
                numStr = "Seven";
                break;
            case 8:
                numStr = "Eight";
                break;
            case 9:
                numStr = "Nine";
            case 10:
                numStr = "Ten";
            case 11:
                numStr = "Jack";
                break;
            case 12:
                numStr = "Queen";
                break;
            case 13:
                numStr = "King";
                break;
        }
        
        return numStr + " of " + mySuit.toString();
    }
}
