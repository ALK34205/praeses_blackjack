/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Random;
/**
 *
 * @author Alec Turner
 */
public class Deck {
    
    /**
     * myCards: an Array of Cards where the top card is the first index
     * numCards: the number of cards currently in the deck.
     * DECK_SIZE: the standard number of cards in a deck defined as a constant
     */
    private Card[] myCards;
    private int numCards;
    private final int DECK_SIZE = 52;
    
    /**
     * Calls the other constructor for one, non shuffled, deck
     */
    public Deck()
    {
        this(1, false);
    }
    
    /**
     * Constructor  defining the number of decks being used and whether it 
     * should be shuffled.
     * @param numDeck :How many sets of 52 cards are being used.
     * @param shuffle :Should the deck(s) be shuffled or not.
     */
    public Deck(int numDecks, boolean shuffle)
    {
        this.numCards = numDecks * DECK_SIZE;
        this.myCards = new Card[this.numCards];
        
        // Card index
        int c = 0;
        
        // for each Deck
        for (int d = 0; d < numDecks; d++) 
        {
            // for each suit
            for (int s = 0; s < 4; s++) 
            {
                // for each number
                for (int n = 1; n <= 13; n++) 
                {
                    //add a new card to the deck
                    this.myCards[c] = new Card(Card.Suit.values()[s], n);
                    c++;
                }
            }
        }
        
        if (shuffle)
        {
            this.shuffle();
        }
    }
    
    /**
     * Uses a random number generator to swap pairs of cards to shuffle
     */
   public void shuffle()
   {
      Random rng = new Random();
      Card temp;
      
      int j;
       for (int i = 0; i < this.numCards; i++)
       {
           j = rng.nextInt(this.numCards);
           
           temp = this.myCards[i];
           this.myCards[i] = this.myCards[j];
           this.myCards[j] = temp;
       }
   }
   
   /**
    * Deals a card from the top of the deck.
    * 
    * @return the card dealt 
    */
   public Card dealCard()
   {
       Card top = this.myCards[0];
       
       for (int c = 1; c < this.numCards; c++)
       {
           this.myCards[c-1] = this.myCards[c];
       }
       this.myCards[this.numCards-1] = null;
       this.numCards--;
       
       return top;
   }
   
   /**
    * Prints the the top cards of the deck.
    * @param numToPrint the number of cards from the top of the deck to print
    */
   public void printDeck(int numToPrint)
   {
       for (int c = 0; c < numToPrint; c++)
       {
           System.out.printf("% 3d/%d %s\n", c+1, this.numCards, 
                   this.myCards[c].toString());
       }
       System.out.printf("\t[%d others]\n", this.numCards-numToPrint);
   }
}
